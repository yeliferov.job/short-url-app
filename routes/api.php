<?php

use App\Http\Controllers\Url\ShortUrlController;
use App\Http\Middleware\TerminatingLog;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group([
    'prefix' => 'v1',
    'middleware' => [
        'throttle:50,1',
        TerminatingLog::class
    ]
], function () {
    Route::group([
        'prefix' => 'url',
    ], function() {
        Route::get('/', [ShortUrlController::class, 'index'])->name('url.get');
        Route::post('/', [ShortUrlController::class, 'store'])->name('url.store');
        Route::delete('/', [ShortUrlController::class, 'destroy'])->name('url.delete');
    });
});
