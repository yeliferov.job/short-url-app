<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;

class ShortUrlApiTest extends TestCase
{
    /**
     * Generated short url response from server
     *
     * @var string
     */
    private static string $shortUrl;

    /**
     * A basic feature test example.
     */
    public function test_store(): void
    {
        $response = $this->postJson(route('url.store'));
        $response->assertStatus(422);

        $response = $this->postJson(route('url.store'), [
            'url' => null
        ]);
        $response->assertStatus(422);

        $response = $this->postJson(route('url.store'), [
            'url' => 'goo.gl'
        ]);
        $response->assertStatus(422);

        $response = $this->postJson(route('url.store'), [
            'url' => 'http://some.url/' . Str::random(2000 - 15)
        ]);
        $response->assertStatus(422);

        $response = $this->postJson(route('url.store'), [
            'url' => 'http:/google.com'
        ]);
        $response->assertStatus(422);

        $response = $this->postJson(route('url.store'), [
            'url' => config('app.url')
        ]);
        $response->assertStatus(422);


        $response = $this->postJson(route('url.store'), [
            'url' => 'http://some.url/' . Str::random(2000 - 16)
        ]);
        $response->assertSuccessful();

        $response = $this->postJson(route('url.store'), [
            'url' => 'https://google.com'
        ]);
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'short_url'
        ]);

        self::$shortUrl = $response->json('short_url');
    }

    public function test_find() {
        $response = $this->get(route('url.get'));
        $response->assertStatus(422);

        $response = $this->call('GET', route('url.get'), [
            'url' => 'https://google.com'
        ]);
        $response->assertNotFound();

        $response = $this->call('GET', route('url.get'), [
            'url' => self::$shortUrl
        ]);
        $response->assertSuccessful();
        $response->assertJsonStructure([
            'url'
        ]);
    }

    public function delete_test() {
        $response = $this->delete(route('url.delete'));
        $response->assertStatus(422);

        $response = $this->delete(route('url.delete'), [
            'url' => 'string'
        ]);
        $response->assertStatus(422);

        $response = $this->delete(route('url.delete'), [
            'url' => 'https://test.test'
        ]);
        $response->assertNotFound();

        $response = $this->delete(route('url.delete'), [
            'url' => self::$shortUrl
        ]);
        $response->assertSuccessful();
    }
}
