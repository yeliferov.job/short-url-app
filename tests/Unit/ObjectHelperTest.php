<?php

namespace Tests\Unit;

use Tests\TestCase;

class ObjectHelperTest extends TestCase
{
    /**
     * A serialize_deep_clone helper unit test.
     */
    public function test_serialize_deep_clone(): void
    {
        $object = json_decode('{"value":"10","model":{"value":"20"},"array":[{"model":{"value":"30"}}]}');
        $objectCopy = serialize_deep_clone($object);
        $objectCopy->value = 100;
        $objectCopy->model->value = 100;
        $objectCopy->array[0]->model->value = 100;

        $this->assertFalse($object->value === $objectCopy->value);
        $this->assertFalse(spl_object_hash($object->model) === spl_object_hash($objectCopy->model));
        $this->assertFalse(spl_object_hash($object->array[0]->model) === spl_object_hash($objectCopy->array[0]->model));
    }

    /**
     * A recursive_deep_clone helper unit test.
     */
    public function test_recursive_deep_clone(): void
    {
        $object = json_decode('{"value":"10","model":{"value":"20"},"array":[{"model":{"value":"30"}}]}');
        $objectCopy = recursive_deep_clone($object);
        $objectCopy->value = 100;
        $objectCopy->model->value = 100;
        $objectCopy->array[0]->model->value = 100;

        $this->assertFalse($object->value === $objectCopy->value);
        $this->assertFalse(spl_object_hash($object->model) === spl_object_hash($objectCopy->model));
        $this->assertFalse(spl_object_hash($object->array[0]->model) === spl_object_hash($objectCopy->array[0]->model));
    }
}
