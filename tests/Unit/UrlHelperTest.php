<?php

namespace Tests\Unit;

use Tests\TestCase;

class UrlHelperTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_get_hash_from_short_url(): void
    {
        $this->assertTrue(get_hash_from_short_url('http://test.test/aFgh5') === 'aFgh5');
        $this->assertTrue(get_hash_from_short_url('http://test.test/aFgh5/') === 'aFgh5');
        $this->assertFalse(get_hash_from_short_url('http://test.test/aFgh5//') === 'aFgh5');
        $this->assertTrue(get_hash_from_short_url('/aFgh5/') === 'aFgh5');
        $this->assertTrue(get_hash_from_short_url('null') === 'null');
    }
}
