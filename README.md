<h1 align="center">Short url app</h1>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About

This app helps to create short url links with minimal percent of collisions.

[//]: # (- [RU docs]&#40;https://laravel.com/docs/routing&#41;.)

## Get started

1. Copy and rename to `.env` file from `.env.docker` or run `cp .env.docker .env`
2. Run `docker-compose build app`
3. Run `docker compose up -d`
4. Run `docker-compose exec app composer install`
5. Run `docker-compose exec app php artisan key:generate` 
6. Run `docker-compose exec app php artisan migrate`
7. Open http://localhost:8050/api/docs

## Commands

To install composer dependencies `docker-compose exec app composer install`
To run tests `docker-compose exec app php artisan test`

## Api Documentation

Documentation available on page http://localhost:8050/api/docs.
To update docs run `docker-compose exec app php artisan l5-swagger:generate`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
