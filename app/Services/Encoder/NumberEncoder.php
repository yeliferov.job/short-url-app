<?php

namespace App\Services\Encoder;

interface NumberEncoder
{
    /**
     * @param mixed ...$number
     * @return string
     */
    public function encode(...$number): string;
}
