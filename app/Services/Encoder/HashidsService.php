<?php

namespace App\Services\Encoder;

use Hashids\Hashids;

class HashidsService extends Hashids implements NumberEncoder
{
    public function __construct()
    {
        $salt = config('services.encoder.salt', '');
        $minHashLength = config('services.encoder.minHashLength', 0);
        $alphabet = config('services.encoder.alphabet');

        parent::__construct($salt, $minHashLength, $alphabet);
    }
}
