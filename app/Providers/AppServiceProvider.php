<?php

namespace App\Providers;

use App\Models\ShortUrl;
use App\Observers\ShortUrlObserver;
use App\Services\Encoder\HashidsService;
use App\Services\Encoder\NumberEncoder;
use App\Services\Encoder\IdIdEncoderService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('id_encoder',function() : NumberEncoder {
            return new HashidsService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        ShortUrl::observe(ShortUrlObserver::class);
    }
}
