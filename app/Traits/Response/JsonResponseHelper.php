<?php

namespace App\Traits\Response;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

trait JsonResponseHelper
{
    public function okResponse(array | Collection $data = []) : JsonResponse
    {
        return response()->json($data, 200);
    }

    public function forbiddenResponse(array | Collection $data = []) : JsonResponse
    {
        return response()->json($data, 403);
    }

    public function notFoundResponse(array | Collection $data = []) : JsonResponse
    {
        return response()->json($data, 404);
    }
}
