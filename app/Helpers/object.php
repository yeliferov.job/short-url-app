<?php
if (!function_exists('serialize_deep_clone')) {
    function serialize_deep_clone(object $object): object {
        return unserialize(serialize($object));
    }

    if (!function_exists('recursive_deep_clone')) {
        function recursive_deep_clone(mixed $object) {
            $result = is_object($object) ? clone $object : $object;
            if (is_array($result)) {
                foreach ($result as $key => $value) {
                    $result[$key] = recursive_deep_clone($value);
                }
            } else if (is_object($object)) {
                foreach ($result as $key => $value) {
                    $result->{$key} = recursive_deep_clone($value);
                }
            }

            return $result;
        }
    }
}
