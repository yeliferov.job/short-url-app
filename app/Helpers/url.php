<?php
if (!function_exists('get_hash_from_short_url')) {
    function get_hash_from_short_url(string $full_url) {
        $prepared = preg_replace('/\/$/', '', $full_url);
        $alphabet = config('services.encoder.alphabet');
        preg_match("/[$alphabet]+$/", $prepared, $matches);

        return $matches[0] ?? '';
    }
}
