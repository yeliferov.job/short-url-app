<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsShortUrlFormat implements ValidationRule
{
    /**
     * Indicates whether the rule should be implicit.
     *
     * @var bool
     */
    public $implicit = true;

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $shortUrlHash = get_hash_from_short_url($value);
        $minHashLength = (int) config('services.encoder.minHashLength');

        if (strlen($value) < $minHashLength) {
            $fail('The :attribute has wrong short url length.');
        }

        $alphabet = config('services.encoder.alphabet');
        preg_match_all("/[^$alphabet]+/m", $shortUrlHash, $matches);

        if (!empty($matches[0])) {
            $fail('The :attribute has wrong short url format.');
        }
    }
}
