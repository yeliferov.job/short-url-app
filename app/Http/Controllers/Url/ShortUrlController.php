<?php

namespace App\Http\Controllers\Url;

use App\Http\Controllers\Controller;
use App\Http\Requests\Url\ShortUrlRequest;
use App\Http\Requests\Url\SrcUrlRequest;
use App\Models\ShortUrl;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class ShortUrlController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/v1/url",
     *     operationId="index",
     *     tags={"Url"},
     *     summary="Get full url by short url",
     *     description="",
     *     @OA\Parameter(
     *         name="url",
     *         in="query",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return object { 'short_url' }"
     *     )
     * )
     * @param ShortUrlRequest $request
     * @return JsonResponse
     */
    public function index(ShortUrlRequest $request) : JsonResponse {
        $srcUrl = Cache::get($request->hash, function () use ($request) {
            $model = ShortUrl::shortUrl($request->hash)->firstOrFail();

            return $model->src_url;
        });

        return $this->okResponse([
            'url' => $srcUrl,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/url",
     *     operationId="store",
     *     tags={"Url"},
     *     summary="Create short url record",
     *     description="",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 required={"url"},
     *                 @OA\Property(
     *                     property="url",
     *                     type="string",
     *                     description="Url string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return object { 'short_url' }"
     *     )
     * )
     *
     * @param SrcUrlRequest $request
     * @return JsonResponse
     */
    public function store(SrcUrlRequest $request): JsonResponse
    {
        ['url' => $url] = $request->validated();

        $shortUrl = Cache::remember($url, config('cache.ttl'), function () use ($url) {
            $model = ShortUrl::firstOrCreate([
                'src_url' => $url
            ]);
            return $model->short_url;
        });

        return $this->okResponse([
            'short_url' => route('web.home', [
                'url' => $shortUrl,
            ]),
        ]);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/url",
     *     operationId="destroy",
     *     tags={"Url"},
     *     summary="Delete short url record",
     *     description="",
     *     @OA\Parameter(
     *         name="url",
     *         in="query",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Return object { 'short_url' }"
     *     )
     * )
     *
     * @param ShortUrlRequest $request
     * @return JsonResponse
     */
    public function destroy(ShortUrlRequest $request): JsonResponse
    {
        $shortUrl = ShortUrl::shortUrl($request->hash)->firstOrFail();
        $shortUrl->delete();

        return $this->okResponse();
    }
}
