<?php

namespace App\Http\Controllers;

use App\Traits\Response\JsonResponseHelper;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Short url app API Documentation",
 *      description="",
 *      @OA\Contact(
 *          email="vladel.name@yandex.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 * @OA\Tag(
 *     name="Projects",
 *     description="API Endpoints of Projects"
 * )
 */
class Controller extends BaseController
{
    use JsonResponseHelper;
}
