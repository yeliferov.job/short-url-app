<?php

namespace App\Http\Controllers;

use App\Models\ShortUrl;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index($url) {
        $srcUrl = Cache::remember($url, config('cache.ttl'), function () use ($url) {
            $model = ShortUrl::shortUrl($url)->firstOrFail();
            return $model->src_url;
        });

        return  response()->redirectTo($srcUrl);
    }
}
