<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TerminatingLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        return $next($request);
    }

    /**
     * Log all request and response after the response has been sent to the browser.
     */
    public function terminate(Request $request, Response $response): void
    {
        logger()->info('Request info: ' . $request->method() . ' ' . $request->url(), [
            'request' => $request->all(),
            'response' => $response
        ]);
    }
}
