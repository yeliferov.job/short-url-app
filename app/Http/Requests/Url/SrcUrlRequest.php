<?php

namespace App\Http\Requests\Url;

use App\Http\Requests\JsonRequest;
use Illuminate\Validation\Validator;

class SrcUrlRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'url' => ['required', 'max:2000', 'url']
        ];
    }

    /**
     * Configure the validator instance.
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            if (str_contains($this->url, config('app.url'))) {
                $validator->errors()->add('url', e('Url should not contains this service url'));
            }
        });
    }
}
