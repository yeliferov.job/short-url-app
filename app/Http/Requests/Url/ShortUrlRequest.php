<?php

namespace App\Http\Requests\Url;

use App\Http\Requests\JsonRequest;
use App\Rules\IsShortUrlFormat;
use Illuminate\Validation\Validator;

class ShortUrlRequest extends JsonRequest
{
    /**
     * Short url hash
     *
     * @var string
     */
    public string $hash;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'url' => ['required', 'max:255', 'url', new IsShortUrlFormat]
        ];
    }

    /**
     * Configure the validator instance.
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            if (is_string($this->url)) {
                $this->hash = get_hash_from_short_url($this->url);
            }
        });
    }
}
