<?php

namespace App\Models;

use Carbon\Carbon;
use Database\Factories\ShortUrlFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Заявка на доп.выпуск акций эмитентом
 * Class RequestShare
 *
 * @package Status\Models
 *
 * @property integer $id Record ID
 * @property string $src_url Source full url
 * @property string $short_url Short url part
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class ShortUrl extends Model
{
    use HasFactory;

    protected $table = 'short_urls';
    protected $primaryKey = 'id';

    protected $fillable = ['src_url', 'short_url'];

    /**
     * Short Url Scope
     *
     * @param Builder $query
     * @param string $short_url
     * @return Builder
     */
    public function scopeShortUrl(Builder $query, string $short_url) : Builder {
        return $query->where('short_url', $short_url);
    }

    /**
     * Short Url Scope
     *
     * @param Builder $query
     * @param string $short_url
     * @return Builder
     */
    public function scopeSrctUrl(Builder $query, string $src_url) : Builder {
        return $query->where('src_url', $src_url);
    }

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return ShortUrlFactory::new();
    }
}
