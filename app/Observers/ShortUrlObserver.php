<?php

namespace App\Observers;

use App\Models\ShortUrl;
use Illuminate\Support\Facades\Cache;

class ShortUrlObserver
{
    /**
     * Handle the ShortUrl "created" event.
     */
    public function created(ShortUrl $shortUrl): void
    {
        $service = resolve('id_encoder');
        $shortUrl->short_url = $service->encode($shortUrl->id);
        $shortUrl->saveQuietly();
    }

    /**
     * Handle the ShortUrl "updated" event.
     */
    public function updated(ShortUrl $shortUrl): void
    {
        //
    }

    /**
     * Handle the ShortUrl "deleted" event.
     */
    public function deleted(ShortUrl $shortUrl): void
    {
        Cache::forget($shortUrl->short_url);
        Cache::forget($shortUrl->src_url);
    }

    /**
     * Handle the ShortUrl "restored" event.
     */
    public function restored(ShortUrl $shortUrl): void
    {
        //
    }

    /**
     * Handle the ShortUrl "force deleted" event.
     */
    public function forceDeleted(ShortUrl $shortUrl): void
    {
        //
    }
}
